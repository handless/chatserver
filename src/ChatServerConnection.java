

import java.io.*;
import java.net.*;
import java.util.Vector;


/*
 * ��������� �����, ������� ����������� �� ����������� ServerLink � ���������� Connectable, 
 * � ServerSocket - ��� ����������� ����� �����
 * ��������� Connectable ��������� ������, ������� ��������� ������
 * ��, �����, ���� ������/������, �������� �� �������
 */
public class ChatServerConnection<E extends ServerLink> implements Connectable<ServerSocket> {
/*
	��������� ���������� ���� �����������, ��� �������� ������ ��������� �������
 */
	private ServerSocket serverSocket = null;
/*
	���������� �������� ���� �������, ������� ������������ ��� ��������� ������� � �������
 */
	private int port;
/*
	������ ����������� ��������
 */
	private Vector<E> clients = null;
	private ServerLinkCreator<E> serverLinkCreator = null;
/*
	��� ����������� ����, ������� ����� ������ ��� �������� � ������� + �������������� ������ ��������
 */
	public ChatServerConnection(int port){
		this.port = port;
		clients = new Vector<E>();
	}
	/*
	 * ����� ������� ����� ������� � ����������� ��� � �����
	 */
	public boolean connect(){
		try{
			serverSocket = new ServerSocket(port);
			return true;
		} catch (Exception e){
			e.printStackTrace();
		}
		return false;
	}
	/*
	 * ��������� ����� �� ����������� �������
	 */
	public E acceptConnection(){
		try{
			/*
			 * �������� ����� ������������� �������
			 * ��� ����� �����? ��� ����� ��+����
			 */
			Socket socket = serverSocket.accept();
			/*
			 * ��������� ������� � ������
			 */
			return addLink(socket);
		} catch(Exception e){
			return null;
		}	
	}
	
	public ServerSocket getSocket(){
		return serverSocket;
	}
	
	/*
	 * �������� ������
	 */
	private void closeServerSocket() throws IOException{
		
		if (serverSocket != null){
			serverSocket.close();
			serverSocket = null;
		}
		
	}
	
	/*
	 * ����� ���������� ServerLinkCreator, 
	 * ������������ ��� �������� ServerLink, ����� ������ ���������� � �������
	 */
	public ServerLinkCreator<E> getServerLinkCreator(){
		return serverLinkCreator;
	}
	
	public void setServerLinkCreator(ServerLinkCreator<E> linkCreator){
		this.serverLinkCreator = linkCreator;
	}
	
	/*
	 * ���������� �������
	 */
	private E addLink(Socket socket){
		if(getServerLinkCreator() == null) throw new RuntimeException("ServerLinkCreator == null");
		
		E client = null;
		try{
			client = getServerLinkCreator().create(socket);
		}catch(Exception e){
			e.printStackTrace();
			client = null;
			return null;
		}
		/*
		 * ���� ��������� ������ ���������� ������� � ������, �� ��������� null
		 */
		return clients.add(client) ? client : null;
	}
	/*
	 * ���������� ������ ��������
	 */
	public Vector<E> getLinks(){
		return clients;
	}
	
	public E getLink(int index){
		synchronized (clients) {
			if ((index >= 0) && (index < clients.size())){
				return clients.get(index);
			} else{
				return null;
			}
		}
		
	}
	/*
	 * ���������� ���������� ������������ ��������
	 */
	public int getLinkCount(){
		return clients.size();
	}
	
	public boolean disconnectLink(E link){
		if(link.disconnect()){
			return clients.remove(link);
		}else{
			return false;
		}
	}
	/*
	 * ��������� ���� ������������ ��������
	 */
	public void disconnectAllLinks(){
		synchronized (clients) {
			/*
			 * ���� �������������� ��� :)
			 * 
			 * ������ �� ������ ������������ �������� � ��������� ��
			 */
			for(E tmp: clients){
				tmp.disconnect();
			}
			/*
			 * ������� ������
			 */
			clients.clear();
			/*
			 * �������������� ����� ������ ��������
			 */
			clients = new Vector<E>();
		}
	}
	
	/*
	 * ���������� ������ �������
	 */
	public boolean disconnect(){
		/*
		 * ��������� ������� �� ������
		 */
		if(!isConnected()) return true;
		/*
		 * ������� ������ ������������ ��������
		 */
		this.disconnectAllLinks();
		try {
			closeServerSocket();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			if(serverSocket != null && serverSocket.isClosed()){
				serverSocket = null;
			}
			return false;
		}
	}
	
	/*
	 * ���������� true ���� ������ �������
	 */
	public boolean isConnected(){
		if (serverSocket != null)
			return !serverSocket.isClosed();
		return true;
	}
	
	public InetAddress getRemoteIP(){
		return null;
	}
	/*
	 * ���������� �� �������
	 */
	public InetAddress getLocalIP(){
		if(isConnected()){
			return getSocket().getInetAddress();
		}
		return null;
	}
	/*
	 * 
	 */
	public int getRemotePort(){
		return -1;
	}
	/*
	 * ���������� ���� �������
	 */
	public int getLocalPort(){
		if(getSocket() != null)
			return getSocket().getLocalPort();
		return port;
	}
	/*
	 * ���������� true �.�. ��� ������))
	 */
	public boolean isServer(){
		return true;
	}
	@Override
	public InetAddress getLocalIp() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public InetAddress getRemoteIp() {
		// TODO Auto-generated method stub
		return null;
	}
}
