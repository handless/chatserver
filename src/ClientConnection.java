

import java.io.*;
import java.net.*;

/*
 * ����� ������������� ����������� �������� ������� � ��������
 */
public class ClientConnection implements Connectable<Socket>{
	protected Socket socket = null;
	//TODO the Protocol interface
	//private Protocol protocol = null;
	/*
	 * ������ �� �������
	 */
	private final InetAddress serverIp;
	/*
	 * ������ ���� �������
	 */
	private final int serverPort;
	/*
	 * 
	 */
	protected ClientConnection(){
		this.serverIp = null;
		this.serverPort = -1;
	}
	/*
	 * ������������� �� � ����� �������
	 */
	public ClientConnection (InetAddress serverIp, int serverPort){
		this.serverIp = serverIp;
		this.serverPort = serverPort;
	}
	/*
	 * �������� ��������� �������
	 */
	public int send(String msg) throws IOException {
		/*
		 * ��������� ����� ������
		 */
		OutputStreamWriter out = new OutputStreamWriter(getSocket().getOutputStream());
		/*
		 * �������� ���������
		 */
		out.write(msg);
		out.flush();
		return msg.length();
	}
	/*
	 * ��������� ������� � �������� � ����� ��� ������������� � ��������
	 */
	@Override
	public boolean connect() {
		try{
			socket = new Socket(serverIp,serverPort);
			return socket.isConnected();
		}catch(IOException e){
			e.printStackTrace();
			return false;
		}
		
	}
	/*
	 * ���������� true ���� ����� �� ������ � ��������� � �������
	 */
	public boolean isConnected(){
		if(getSocket()!=null){
			return getSocket().isConnected() && !getSocket().isClosed();
		}
		return false;
	}
	/*
	 *���������� ������� �� �������
	 */
	@Override
	public boolean disconnect() {
		/*
		 * �������� �� ������� � ��������
		 */
		if(!isConnected()){
			return true;
		}
		try {
			/*
			 * ���� ����� ������, �� ��������� ���
			 */
			if(getSocket()!=null){
				getSocket().close();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		
		return !isConnected();
	}
	/*
	 * ���������� �����
	 */
	@Override
	public Socket getSocket() {
		return socket;
	}
/*
 * ���������� �� �������
 */
	@Override
	public InetAddress getLocalIp() {
		if (getSocket()!=null){
			return getSocket().getLocalAddress();
		}
		return null;
	}
/*
 * ���������� �� �������
 */
	@Override
	public InetAddress getRemoteIp() {
		if (getSocket()!=null){
			return getSocket().getInetAddress();
		}
		return null;
	}
/*
 * ���������� ���� �������
 */
	@Override
	public int getLocalPort() {
		if (getSocket()!=null){
			return getSocket().getLocalPort();
		}
		return -1;
	}
/*
 * ���������� ���� �������
 */
	@Override
	public int getRemotePort() {
		if (getSocket()!=null){
			return getSocket().getPort();
		}
		return serverPort;
	}

	@Override
	public boolean isServer() {
		return false;
	}
	/*
	 * �������� �� ������� ��������� ���������
	 */
	public int hasInput()
	{
		if(getSocket() == null)
			return -1;
		try
		{
			//���������� ���������� ���� ������
			return getSocket().getInputStream().available();
		}
		catch(IOException e)
		{
			return -1;
		}
	}
	/*
	 * ������ �������� ������
	 */
	public byte[] readInput() throws IOException
	{
		if(getSocket() == null)
			new IOException();
		
		//Don't use a BufferedInputStream, by experience I found some problems
		//�������� �������� �����
		InputStream in = getSocket().getInputStream();
		//�������� �� ������� �������� ������, ��� �� ������ ������ 0
		if(in.available() > 0)
		{
			//������� ������ �������� ������ ���������� ���� �������� ������
			byte[] input = new byte[in.available()];		//TODO Not the best way if in.available is big
			//��������� ������������ ������ � ��������� ������ ��������� ������
			int reads = in.read(input, 0, input.length);

			if(reads < input.length)	//Just to be sure, not necessary I think
			{
				byte[] input_ = new byte[reads];
				System.arraycopy(input, 0, input_, 0, reads);
				input = input_;
			}
			return input;
		}
		return null;
	}
}
